package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);

		Button b1 = (Button) findViewById(R.id.calmon);
		b1.setOnClickListener(this);

		Button b2 = (Button) findViewById(R.id.button1);
		b2.setOnClickListener(this);
		
		
		
		TextView showr = (TextView) findViewById(R.id.showrate);
		showr.setText("Rate = " + exrate);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}

	float exrate = (float) 10;

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		int id = v.getId();

		if (id == R.id.calmon) {

			EditText money = (EditText) findViewById(R.id.editText1);
			EditText editTexttime = (EditText) findViewById(R.id.editText2);

			TextView output = (TextView) findViewById(R.id.output);
			TextView showr = (TextView) findViewById(R.id.showrate);

			try {
				float getmon = Float.parseFloat(money.getText().toString());
				float time = Float
						.parseFloat(editTexttime.getText().toString());

				float calmon = getmon
						* (float) Math.pow((1 + exrate / 100), time);

				output.setText(String.format(Locale.getDefault(), "%.2f",
						calmon));

				showr.setText("Rate = " + exrate);

			} catch (NumberFormatException e) {

			} catch (NullPointerException e) {

			}

		}

		else if (id == R.id.button1) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("exrate", exrate);

			startActivityForResult(i, 9998);

		}

	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9998 && resultCode == RESULT_OK) {
			
			exrate = data.getFloatExtra("interestRate", 10.0f);
			TextView showr = (TextView) findViewById(R.id.showrate);
			showr.setText("Rate = " + exrate);
			TextView tvRate = (TextView) findViewById(R.id.output);
			tvRate.setText(String.format(Locale.getDefault(), "%.2f", exrate));
		}
	}

}
